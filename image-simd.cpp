#include <hpcdefs.hpp>
#include <image.hpp>

void convert_rgb_to_grayscale_optimized(const uint8_t *CSE6230_RESTRICT rgb_image, uint8_t *CSE6230_RESTRICT grayscale_image, size_t width, size_t height) {
	size_t j = 0;
	for (size_t i = 0; i < height; i++) {
		for (j=0; j< width; j+=16) {
			const __m128i chunk0 = _mm_loadu_si128((const __m128i*)&rgb_image[(i*width+j)*3]);
			const __m128i chunk1 = _mm_loadu_si128((const __m128i*)&rgb_image[(i*width+j)*3 + 16]);
			const __m128i chunk2 = _mm_loadu_si128((const __m128i*)&rgb_image[(i*width+j)*3 + 32]);
			
			__m128i ssse3_red_indeces_0 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 15, 12, 9, 6, 3, 0);
			__m128i ssse3_red_indeces_1 = _mm_set_epi8(-1, -1, -1, -1, -1, 14, 11, 8, 5, 2, -1, -1, -1, -1, -1, -1);
			__m128i ssse3_red_indeces_2 = _mm_set_epi8(13, 10, 7, 4, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
			__m128i ssse3_green_indeces_0 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 13, 10, 7, 4, 1);
			__m128i ssse3_green_indeces_1 = _mm_set_epi8(-1, -1, -1, -1, -1, 15, 12, 9, 6, 3, 0, -1, -1, -1, -1, -1);
			__m128i ssse3_green_indeces_2 = _mm_set_epi8(14, 11, 8, 5, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
			__m128i ssse3_blue_indeces_0 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, 11, 8, 5, 2);
			__m128i ssse3_blue_indeces_1 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, 13, 10, 7, 4, 1, -1, -1, -1, -1, -1);
			__m128i ssse3_blue_indeces_2 = _mm_set_epi8(15, 12, 9, 6, 3, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
			
			__m128i red = _mm_or_si128(_mm_or_si128(_mm_shuffle_epi8(chunk0, ssse3_red_indeces_0),
					_mm_shuffle_epi8(chunk1, ssse3_red_indeces_1)), _mm_shuffle_epi8(chunk2, ssse3_red_indeces_2));
			__m128i green = _mm_or_si128(_mm_or_si128(_mm_shuffle_epi8(chunk0, ssse3_green_indeces_0),
					_mm_shuffle_epi8(chunk1, ssse3_green_indeces_1)), _mm_shuffle_epi8(chunk2, ssse3_green_indeces_2));
			__m128i blue = _mm_or_si128(_mm_or_si128(_mm_shuffle_epi8(chunk0, ssse3_blue_indeces_0),
					_mm_shuffle_epi8(chunk1, ssse3_blue_indeces_1)), _mm_shuffle_epi8(chunk2, ssse3_blue_indeces_2));

			 __m128i zero8 = _mm_set1_epi8(0);
                        __m128i red_lo = _mm_unpacklo_epi8(red, zero8);
                        __m128i red_hi = _mm_unpackhi_epi8(red, zero8);
			__m128i green_lo = _mm_unpacklo_epi8(green, zero8);
                        __m128i green_hi = _mm_unpackhi_epi8(green, zero8);
			__m128i blue_lo = _mm_unpacklo_epi8(blue, zero8);
                        __m128i blue_hi = _mm_unpackhi_epi8(blue, zero8);
			
			__m128i red_factor = _mm_set1_epi16(54);
			__m128i green_factor = _mm_set1_epi16(183);
			__m128i blue_factor = _mm_set1_epi16(19);
			
			__m128i luma_lo = _mm_add_epi16(_mm_add_epi16(_mm_mullo_epi16(red_lo, red_factor), _mm_mullo_epi16(green_lo, green_factor)), 
					_mm_mullo_epi16(blue_lo, blue_factor));
			__m128i luma_hi = _mm_add_epi16(_mm_add_epi16(_mm_mullo_epi16(red_hi, red_factor), _mm_mullo_epi16(green_hi, green_factor)),
					_mm_mullo_epi16(blue_hi, blue_factor));
			luma_lo = _mm_srli_epi16 (luma_lo, 8);
			luma_hi = _mm_srli_epi16 (luma_hi, 8);
			__m128i luma = _mm_packus_epi16 (luma_lo, luma_hi);
						
			_mm_storeu_si128((__m128i *) &grayscale_image[(i * width + j)], luma);
		}
		for(;j<width;j++){
			   const uint16_t red   = rgb_image[(i * width + j) * 3 + 0];
                           const uint16_t green = rgb_image[(i * width + j) * 3 + 1];
                           const uint16_t blue  = rgb_image[(i * width + j) * 3 + 2];
			   const uint8_t  luma  = (red * 54 + green * 183 + blue * 19) >> 8;
                           grayscale_image[i * width + j] = luma;
		}
		
	}
}

void integrate_image_optimized(const uint8_t *CSE6230_RESTRICT source_image, uint32_t *CSE6230_RESTRICT integral_image, size_t width, size_t height) {
	size_t s_index = 0;
	for(size_t i=0;i<height; i++) {
		__m128i prefix_sum = _mm_set1_epi32(0);
		int width_s = width;
		for(; width_s>=16; width_s -= 16) {
			__m128i elements = _mm_loadu_si128((const __m128i*)&source_image[s_index]);
			__m128i zero8 = _mm_set1_epi8(0);
			__m128i elements16[2];
			elements16[0] = _mm_unpacklo_epi8(elements, zero8);
			elements16[1] = _mm_unpackhi_epi8(elements, zero8);
			for(int j=0;j<2; j++){
                                __m128i integral_part1 = _mm_add_epi16(_mm_slli_si128(elements16[j], 2), elements16[j]);
                                __m128i integral_part2 = _mm_add_epi16(integral_part1, _mm_slli_si128(integral_part1, 4));
				elements16[j] = _mm_add_epi16(integral_part2, _mm_slli_si128(integral_part2, 8));
                        }
			__m128i zero16 = _mm_set1_epi16(0);
			__m128i elements32[4];
			elements32[0] = _mm_add_epi32(_mm_unpacklo_epi16(elements16[0], zero16), prefix_sum);
			elements32[1] = _mm_add_epi32(_mm_unpackhi_epi16(elements16[0], zero16), prefix_sum);
			__m128i prefix_sum32 = _mm_set1_epi32(_mm_extract_epi32(elements32[1],3));
			elements32[2] = _mm_add_epi32(_mm_unpacklo_epi16(elements16[1], zero16), prefix_sum32);
                        elements32[3] = _mm_add_epi32(_mm_unpackhi_epi16(elements16[1], zero16), prefix_sum32);
			for(int k=0;k<4;k++)
				_mm_storeu_si128((__m128i *) &integral_image[s_index+(k*4)], elements32[k]);
			prefix_sum = _mm_set1_epi32(_mm_extract_epi32(elements32[3],3));
                        s_index += 16;
		}
		uint32_t integral_sum = _mm_extract_epi32(prefix_sum,0);
                for (;width_s>0; width_s--) {
                        integral_sum += source_image[s_index];
                        integral_image[s_index] = integral_sum;
			s_index++;
                }
	}
	for (size_t j = 0; j < width; j++) {
                uint32_t integral = 0;
                for (size_t i = 0; i < height; i++) {
                        integral += integral_image[i * width + j];
                        integral_image[i * width + j] = integral;
                }
        }
}
